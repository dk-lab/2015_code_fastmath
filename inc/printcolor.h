#ifndef FASTMATH_PRINTCOLOR_H
#define FASTMATH_PRINTCOLOR_H

#include <stdio.h>

namespace fastmath {

void printf_cyan();
void printf_red();
void printf_yel();
void printf_mag();
void printf_std();

}

#endif
